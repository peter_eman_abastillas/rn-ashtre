import * as firebase from "firebase";

export default class Firebase {

    /**
     * Initialises Firebase
     */
    static initialise() {
        firebase.initializeApp({
            apiKey: "AIzaSyD81Le_GWistKTCYVCv42GDfjjljR_sB18",
            authDomain: "ashtre-a0aca.firebaseapp.com",
            databaseURL: "https://ashtre-a0aca.firebaseio.com",
            projectId: "ashtre-a0aca",
            storageBucket: "ashtre-a0aca.appspot.com",
            messagingSenderId: "1042765044407"
        });
    }

}
