import * as firebase from "firebase";
import GeoFire from 'geofire'

const REF = "/yosi/location";
const REF_GEO = "/yosi/goeloc";

export default class Database {

    /**

     * Sets a users mobile number
     * @param params
     * @returns {firebase.Promise<any>|!firebase.Promise.<void>}
     */
    static saveYosiArea(params, success, error) {
            let id = params.id;
            var ref = firebase
                .database()
                .ref(REF_GEO);

        return firebase
            .database()
            .ref(REF+"/"+id)
            .set(params)
            .then((res)=> {
                let geoFire = new GeoFire(ref);
                geoFire.set(id, params.coordinate).then(() => console.debug(arguments)).catch(error => {
                    console.log(error);
                });
                success();
            })
            .catch((err)=> {
                error(err)
            });

    }

    /**
     * Listen for changes to a users mobile number
     * @param userId
     * @param callback Users mobile number
     */
    static yoisBreack(coordinate, callback) {

        var geofireRef = new GeoFire(firebase.database().ref(REF_GEO));

        let geoQuery = geofireRef.query({
            center: coordinate,
            radius: 2
        });


        var onReadyRegistration = geoQuery.on("ready", ()=> {
            console.log("GeoQuery has loaded and fired all other events for initial data");
        });

        var onKeyEnteredRegistration = geoQuery.on("key_entered", (key, location, distance) => {
            console.log(key + " entered query at " + location + " (" + distance + " km from center)");
            callback(key, location, distance);
        });

        var onKeyExitedRegistration = geoQuery.on("key_exited", (key, location, distance) =>  {
            console.log(key + " exited query to " + location + " (" + distance + " km from center)");
            callback(key, location, distance);
        });

        var onKeyMovedRegistration = geoQuery.on("key_moved", (key, location, distance) =>  {
            console.log(key + " moved within query to " + location + " (" + distance + " km from center)");
        });

    }
    static yosiDetails(data,callback) {

        firebase
            .database()
            .ref(REF+"/"+data)
            .on("value", (snapshot)=> {
                callback(snapshot.val());
            }, (errorObject)=> {
                console.log("The read failed: " + errorObject.code);
            });
    }
    static VoteDetails(id, data, success, error) {
        firebase
            .database()
            .ref(REF+"/"+id+"/vote").update({
                [data.type]: data.num
            }).then((res)=> {
                success();
            })
            .catch(function(err) {
                error(err)
            });
    }
}