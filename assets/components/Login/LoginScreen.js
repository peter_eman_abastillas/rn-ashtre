import React from "react";
import {
    TextInput,
    Text,
    View,
    StyleSheet,
    AsyncStorage,
    dismissKeyboard,
    Image,
    TouchableWithoutFeedback
} from "react-native";

import * as firebase from "firebase";

import Button from "apsl-react-native-button";
import DismissKeyboard from "dismissKeyboard";



export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            response: ""
        };

        this.signup = this.signup.bind(this);
        this.login = this.login.bind(this);
    }

    async signup() {
        var _this = this;
        DismissKeyboard();

        try {
            await firebase
                .auth()
                .createUserWithEmailAndPassword(this.state.email, this.state.password)
                .then((user)=> {
                    _this.setState({
                        response: "account created"
                    });
                    _this.login()
                }, (error)=> {
                    _this.setState({
                        response: error.toString()
                    })
                });





        } catch (error) {
            this.setState({
                response: error.toString()
            })
            console.error(error);

        }

    }

    async login() {

        DismissKeyboard();

        try {
            await firebase
                .auth()
                .signInWithEmailAndPassword(this.state.email, this.state.password)
                .then((userData) => {
                        this.setState({
                            loading: false
                        });
                        AsyncStorage.setItem('userData', JSON.stringify(userData));
                        this.props.navigation.navigate("SplashScreen",  {user:userData})
                    }
                ).catch((error) => {
                    this.setState({
                        loading: false
                    });
                    console.error('Login Failed. Please try again'+error);
                });

        } catch (error) {
            this.setState({
                response: error.toString()
            })
            console.error(error);
        }

    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => {DismissKeyboard()}}>
                <Image source={require('../../icons/smoke-bg.png')} style={styles.container}>
                    <View style={styles.formGroup}>
                        <Text style={styles.title}>AshTr3</Text>
                        <TextInput
                            style={styles.textInput}
                            placeholder={"Email Address"}
                            onChangeText={(email) => this.setState({email})}
                            keyboardType="email-address"
                            autoCapitalize="none"
                        />
                        <TextInput
                            style={styles.textInput}
                            placeholder={"Password"}
                            onChangeText={(password) => this.setState({password})}
                            secureTextEntry={true}
                            autoCapitalize="none"
                        />

                        <View style={styles.submit}>
                            <Button onPress={this.signup} style={styles.buttons} textStyle={{fontSize: 18}}>
                                Sign up
                            </Button>
                            <Button onPress={this.login} style={styles.buttons} textStyle={{fontSize: 18}}>
                                Login
                            </Button>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.response}>{this.state.response}</Text>
                    </View>
                </Image>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({

    formGroup: {
        padding: 50
    },

    title: {
        paddingBottom: 16,
        textAlign: "center",
        color: "#000",
        fontSize: 35,
        fontWeight: "bold",
        opacity: 0.8,
    },

    submit: {
        flexDirection:'row',
        paddingTop: 30
    },

    response: {
        textAlign: "center",
        paddingTop: 0,
        padding: 50
    },
    container: {
        flex: 1,
        backgroundColor: "#8781bd",
        paddingTop: 50,
        width: null,
        resizeMode: 'cover', // or 'stretch'
        height: null
    },
    textInput: {
        color: 'rgba(23,23,23,.5)',
        marginTop:10,
        padding:5,
        borderRadius:5,
        height: 40,
        fontSize: 20,
        backgroundColor: 'rgba(255,255,255,.5)'
    },
    buttons: {
        backgroundColor: "whitesmoke"

    }
});

