import React from 'react';
import { StyleSheet, Image, AsyncStorage, View } from 'react-native';
import MyMapView from './MyMapView';

export default class SplashScreen extends React.Component {
    static navigationOptions = {
        title: 'Ashtr3',

    };
    constructor(props) {
        super(props)
        console.debug(props)
        this.state = {
            user: props.navigation.state.params.user,
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.0041,
                longitudeDelta: 0.0021
            },
        };
    }
    componentDidMount() {
        // AsyncStorage.setItem('userData', JSON.stringify({}));

        this.watchID = navigator.geolocation.watchPosition((position) => {
            // Create the object to update this.state.mapRegion through the onRegionChange function
            let region = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                altitude: 5,
                latitudeDelta: 0.0041,
                longitudeDelta: 0.0021
            }
            this.onRegionChange(region, region.latitude, region.longitude);
        }, (error) => {
            console.error(error);
            // alert(JSON.stringify(error))
        }, {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 1000
        });
    }
    onRegionChange(region) {
        this.setState({
            region: region
        });
    }

    render() {

        if(this.state.region.latitude==null) {
            return null;
        }
        return (

            <MyMapView
                {...this.props}
                style={styles.container}
                region={this.state.region}
                user={this.state.user}
            />
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerRight: {

        height: 30,
        width: 30,
    }
});
