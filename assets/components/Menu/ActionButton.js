import React from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Text,
    View,
    Image,
    Dimensions
} from 'react-native';



var {height, width}  = Dimensions.get('window');
export default class ActionButton extends React.Component {
    render() {
        return(
            <View style={styles.topRightOverlay}>
                <Image source={require('../../icons/logout.png') } resizeMode="cover" style={styles.logOutIcon}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    topRightOverlay: {
        backgroundColor: '#fff',
        minWidth:70,
        height:60,
        position: 'absolute',
        top:30,
        right:-1,
        borderBottomLeftRadius:30,
        borderTopLeftRadius:30,
        alignItems:"center",
        elevation:4
    },
    logOutIcon: {
        marginTop:15/2,
        width:45,
        height:45,
        borderRadius:45/2,
        borderColor:"black",
        borderWidth:1,
        // justifyContent:"center",
        // alignItems:"center",

    }

});
