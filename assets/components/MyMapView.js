import React from 'react';
import { StyleSheet, View, Image, Dimensions, Modal, Text, PermissionsAndroid } from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {
    AdMobBanner
} from 'react-native-admob'
import Database from '../libs/Database'
import CreatePinDetails from './Details/CreatePinDetails';
import ViewPinDetails from './Details/ViewPinDetails';
import InfoModal from './Info/InfoModal';
import ActionButton from './Menu/ActionButton';



var {height, width}  = Dimensions.get('window');

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            infoGraphicModal: true,
            marker: [],
            createModal: false,
            tempMarker: {
                latitude: 0,
                longitude: 0,
            },
            region: new MapView.AnimatedRegion({
                latitude: props.region.latitude,
                longitude: props.region.longitude,
                latitudeDelta: 0.0041,
                longitudeDelta: 0.0021
            }),
            markerDetails: {
                coordinate: {},
                nameOfPlace: "",
                vote:{
                    up:0,
                    down:0
                },
                images: [
                    'https://ak6.picdn.net/shutterstock/videos/11562764/thumb/1.jpg?i10c=img.resize(height:160)'
                ],
                createdAt: "",
                createdBy: "",
                desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum scelerisque eros",
    }
        };
        this.loadMarkers = this.loadMarkers.bind(this);
        this.handlePressMarker = this.handlePressMarker.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
        this.hideInfo = this.hideInfo.bind(this);
    }


    loadMarkers(key, location, distance) {
        let [latitude, longitude] = location;
        let coordinate = {
            "latitude": latitude,
            "longitude": longitude,
        };
        let data = (nameKey, myArray) => {
            for (var i = 0; i < myArray.length; i++) {
                if (myArray[i].id === nameKey) {
                    return true;
                }
            }
            return false
        };
        if(data(key, this.state.marker)) return;

        this.setState({
            marker: [
                ...this.state.marker,
                {
                    id: key,
                    coordinate:coordinate
                }
            ]
        })
    }
    componentDidMount() {
        Database.yoisBreack([this.props.region.latitude, this.props.region.longitude], this.loadMarkers);

    }
    onRegionChange(region) {
        Database.yoisBreack([this.state.region.latitude._value, this.state.region.longitude._value], this.loadMarkers)
        this.state.region.setValue(region);
    }
    handlePressMarker(e) {
        this.setState({
            tempMarker: e.nativeEvent.coordinate,
            createModal:true
        });
        return;
    /*
        this.props.navigation.navigate('CreatePinDetails',
            {
                ...this.props,
                user: this.props.navigation.state.params.user,
                marker: e.nativeEvent.coordinate
            });
*/
    }
    setModalVisible() {
        this.setState({modalVisible: false});
    }
    async openMarker(data) {
        var _this = this;
        Database.yosiDetails(data.id, (data)=> {
            console.debug(data);
            if(!data.images) {
                data.images= [
                    'https://ak6.picdn.net/shutterstock/videos/11562764/thumb/1.jpg?i10c=img.resize(height:160)'
                ]
            }
            if(!data.nameOfPlace) {
                data.nameOfPlace = "No address located"
            }
            _this.setState({
                markerDetails: {
                    ...data
                }
            }, ()=> {
                _this.setState({
                    modalVisible: true
                });
            });

        });
    }
    closeModal() {
        this.setState({
            createModal:false,
        })
    }
    showInfo() {
        this.setState({
            infoGraphicModal:true,
        })
    }
    hideInfo() {
        this.setState({
            infoGraphicModal:false,
        })
    }
    render() {
        return (
            <View style={styles.container}>

                <MapView.Animated
                    style={styles.mapContainer}
                    region={this.props.region}
                    showsUserLocation={true}
                    followUserLocation={true}
                    showsMyLocationButton={true}
                    onRegionChange={this.onRegionChange.bind(this)}
                    onLongPress={this.handlePressMarker}
                    showsCompass={true}
                    moveOnMarkerPress={false}

                >
                    {this.state.marker.map((marker, i)=> {
                        return (
                            <Marker
                                onPress={() => {
                                    this.openMarker(marker)
                                }}
                                key={i} {...marker}>
                                <View
                                    style={styles.markerContainer}
                                >
                                    <Image
                                        style={styles.markerIcon}
                                        source={require('../icons/ashtray.png')}/>
                                </View>
                            </Marker>
                            )
                    })}
                </MapView.Animated>
                {/*<ActionButton {...this.props} />*/}

                <AdMobBanner
                    style={{
                        position:'absolute',
                        bottom:0,
                        left:0,
                        width:width,
                        height:50,
                        backgroundColor: 'rgba(255,233,233,1)'
                    }}
                    adSize="BANNER"
                    adUnitId="ca-app-pub-1853269249171153/5769081445"
                    bannerSize="BANNER"
                    didFailToReceiveAdWithError={(e) => { console.error('didFailToReceiveAdWithError', e); }}
                    adViewDidReceiveAd={(e) => {console.log('adViewDidReceiveAd', e)}}
                />

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.infoGraphicModal}
                    onRequestClose={()=>{}}
                >
                    <View stlye={[styles.modal, {minHeight:300,height:300}]}>
                        <InfoModal
                            closeModal={this.hideInfo}
                            />
                    </View>
                </Modal>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.createModal}
                    onRequestClose={()=>{}}
                >
                    <View stlye={styles.modal}>

                        <CreatePinDetails
                            closeModal={this.closeModal}
                            {...this.props}
                            user={this.props.navigation.state.params.user}
                            marker={this.state.tempMarker}/>
                    </View>
                </Modal>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={()=>{}}
                >
                    {this.state.modalVisible ? <ViewPinDetails {...this.props} closeModal={this.setModalVisible} markerDetails={this.state.markerDetails} /> : null}

                </Modal>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    modal: {
        elevation: 3,
        marginTop:50,
        marginLeft:20,
        marginRight:20,
        minHeight:500,
        backgroundColor:"#A2E5F9",
        borderRadius:5,
        alignSelf:"stretch",
        padding:0,
        height:null
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mapContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 50,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerIcon: {
        width: 30,
        height: 30,
        resizeMode:'cover',
    },
    markerContainer: {
        padding:5,
        borderRadius:5,
        backgroundColor: '#20BF55'
    },
    textDescription: {
        color: "#0b4f6c",
        fontSize:14,
        margin:10,
    },
    scrollView: {
        maxHeight:210,
        minHeight:210,
        marginTop:10
    },
    button: {
        backgroundColor: "#20BF55",
        position:"absolute",
        bottom:3,
        margin:3,
        padding:3,
        height:40,
        borderRadius:3,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width:width-60,
    },

    description: {
        overflow:'visible',
        backgroundColor: '#fff',
        padding: 10,
        marginBottom:10,
    },
    ImageSlider: {
        borderRadius:5,
        zIndex:-1,
        paddingTop:10,
        paddingBottom:10,
    },
    textHeader : {
        flexDirection:'column',

    },

    textHeaderTitle : {
        fontSize:12,
        color:"#0b4f6c"
    },

    textHeaderOthers : {
        fontSize:12,
        color:"#0b4f6c"
    },
    topRightOverlay: {
        backgroundColor: '#fff',
        width:40, height:40,
        position: 'absolute',
        top:30,
        right:10,

    },
    actionButtonContainer : {
        position: 'absolute',
        top:130,
        right:0,
        overflow:'visible',
        width:140,
        flexDirection:"row"
    },

    actionButton : {
        zIndex:99,
        width:50,
        height:50,
        margin:5
    },

});
