import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';

export default class LoadingScreen extends React.Component {
    static navigationOptions = {
        title: 'Ashtr3',
    };

    render() {

        return (
            <Image source={require('../icons/smoke-bg.png')} style={styles.container}>

            </Image>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
