import React from 'react';
import { StyleSheet, View, Image, Dimensions, Text, TouchableHighlight, ScrollView } from 'react-native';
import Database from '../../libs/Database'
import ImageSlider from 'react-native-image-slider';
const infoGraphicImages = [
    "https://s3.amazonaws.com/ashtre/01.jpg",
    "https://s3.amazonaws.com/ashtre/02.jpg",
    "https://s3.amazonaws.com/ashtre/03.jpg",
    "https://s3.amazonaws.com/ashtre/04.jpg",
    "https://s3.amazonaws.com/ashtre/05.jpg",
    "https://s3.amazonaws.com/ashtre/06.jpg",
    "https://s3.amazonaws.com/ashtre/07.jpg",
    "https://s3.amazonaws.com/ashtre/08.jpg",
    "https://s3.amazonaws.com/ashtre/09.jpg"
];
var {height, width}  = Dimensions.get('window');

export default class ViewPinDetails extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            imageSrc:'',
            num: 0,
            selected: [],
            markerDetails: props.markerDetails,
            enableButton: true

        };
        this.closeModal = this.closeModal.bind(this);
        this.vote = this.vote.bind(this);
    }



    vote(vote, num) {
        var type = vote ? "up" : "down";
        var key = 1;
        if(this.state.markerDetails.vote && this.state.markerDetails.vote[type]) {
            key = this.state.markerDetails.vote[type]+1;
        }
        var data = {
                type: type,
                num: key
            };
        Database.VoteDetails(this.state.markerDetails.id, data, ()=>{
            this.setState({enableButton:false})
        },()=>{})
    }

    closeModal() {
        this.props.closeModal();
    }
    render() {
        return (
            <View style={styles.modal}>

                <View style={styles.ImageSlider}>
                    <ImageSlider  height={width-60}  images={infoGraphicImages}/>
                </View>
                <View style={styles.actionButtonContainer}>
                    <TouchableHighlight
                        style={[styles.actionButton, (this.state.enableButton) ? {}: styles.disableButton ]}
                        disabled={!this.state.enableButton}
                        onPress={() => this.vote(true, 1)}>
                        {this.state.enableButton ? <Image style={styles.actionButtonImage} source={require('../../icons/check.png')}/> : <Image style={styles.actionButtonImage} source={require('../../icons/check-disable.png')}/>}
                    </TouchableHighlight>
                    <TouchableHighlight
                        style={[styles.actionButton, (this.state.enableButton) ? {}: styles.disableButton ]}
                        disabled={!this.state.enableButton}
                        onPress={() => this.vote(false, 1)}>
                        {this.state.enableButton ? <Image style={styles.actionButtonImage} source={require('../../icons/warning.png')}/> :  <Image style={styles.actionButtonImage} source={require('../../icons/warning-disable.png')}/>}
                    </TouchableHighlight>
                </View>
                <View style={styles.description}>
                    <View style={{overflow:'visible'}}>
                        <View style={styles.textHeader} >
                            <View>
                                <Text style={styles.textHeaderTitle}>{this.state.markerDetails.nameOfPlace}</Text>
                            </View>
                            <View>
                                <Text style={styles.textHeaderOthers}>No recent rating</Text>
                            </View>
                        </View>
                    </View>
                    <ScrollView style={styles.scrollView}>
                        <Text style={styles.textDescription}>{this.state.markerDetails.desc}</Text>
                    </ScrollView>
                </View>

                <TouchableHighlight
                    style={styles.button}
                    onPress={()=>this.closeModal()}>
                    <Text>Close</Text>
                </TouchableHighlight>

            </View>

        );
    }

}

const styles = StyleSheet.create({
    modal: {
        elevation: 3,
        marginTop:50,
        marginLeft:20,
        marginRight:20,
        minHeight:400,
        backgroundColor:"#FFF",
        borderRadius:5,
        alignSelf:"stretch",
        padding:0,
        height:null
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    mapContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 50,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerIcon: {
        width: 30,
        height: 30,
        resizeMode:'cover',
    },
    markerContainer: {
        padding:5,
        borderRadius:5,
        backgroundColor: 'rgb(128, 244, 66)'
    },
    textDescription: {
        color: "#0b4f6c",
        fontSize:14,
        margin:10,
    },
    scrollView: {
        maxHeight:170,
        minHeight:150,
        marginTop:10
    },
    button: {
        backgroundColor: "#20BF55",
        position:"absolute",
        bottom:3,
        margin:3,
        padding:3,
        height:40,
        borderRadius:3,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width:width-60,
    },

    description: {
        overflow:'visible',
        backgroundColor: '#fff',
        padding: 10,
        marginBottom:10,
    },
    ImageSlider: {
        borderRadius:5,
        zIndex:-1,
        paddingTop:10,
        paddingBottom:10,
    },
    textHeader : {
        flexDirection:'column',

    },

    textHeaderTitle : {
        fontSize:12,
        color:"#0b4f6c"
    },

    textHeaderOthers : {
        fontSize:12,
        color:"#0b4f6c"
    },
    topRightOverlay: {
        backgroundColor: '#fff',
        width:40, height:40,
        position: 'absolute',
        top:30,
        right:10,

    },
    actionButtonContainer : {
        position: 'absolute',
        top:130,
        right:0,
        overflow:'visible',
        width:140,
        flexDirection:"row"
    },
    disableButton: {
        opacity:.5,
    },
    actionButton : {
        zIndex:99,
        width:50,
        height:50,
        margin:5
    },
    actionButtonImage: {
        width:50,
        height:50,
    }

});
