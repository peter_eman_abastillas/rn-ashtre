import React from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Text,
    View,
    TextInput,
    Dimensions
} from 'react-native';
import uuid from 'react-native-uuid';
import moment from 'moment/min/moment-with-locales.min';
import Database from '../../libs/Database'


var {height, width}  = Dimensions.get('window');
export default class CreatePinDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            loading:false,
            nameOfPlace: null,
            marker: props.marker,
            text: ""
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.cancel = this.cancel.bind(this);
    }
    onSubmit() {
        var _this = this,
            newID = uuid.v4()
            payload = {
                id:newID,
                desc: this.state.text,
                nameOfPlace: this.state.nameOfPlace,
                createdAt: moment().unix(),
                createdBy:  this.props.navigation.state.params.user.uid
            }
        Database.saveYosiArea({...payload, coordinate: [this.state.marker.latitude,  this.state.marker.longitude]},
            ()=>_this.props.closeModal(), (e)=> {
                console.error(e);
            });
    }
    cancel() {
        this.props.closeModal();
    }
    async componentDidMount() {
        var {latitude, longitude} = this.state.marker, _this = this;
        if(latitude==0 && longitude == 0) return;
        await fetch("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude, {
            method:"GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((resp)=>{
            return resp.json()
        })
            .then((resp)=>{
            try {
                console.debug(resp.results[0])
                _this.setState({
                    nameOfPlace: resp.results[0].formatted_address
                })
            } catch (e) {
                console.debug(e)
            }

        }).catch((e)=>console.error(e));
    }
    render() {
        return (
            <View>
                <View style={styles.modalWhite}>
                    <Text style={styles.defaultText}>Say something about this place, or how to get there.</Text>

                    <TextInput
                        style={styles.inputDescription}
                        multiline = {true}
                        autoFocus={true}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        numberOfLines = {4}
                        onChangeText={(text) => this.setState({text})}
                        value={this.state.text}

                    />
                    <Text style={styles.defaultText}>Your currently at: {this.state.nameOfPlace}</Text>
                    <View style={styles.buttonContainer}>
                        <TouchableHighlight onPress={() => this.onSubmit()} style={[styles.submit,styles.button]}>
                            <Text style={styles.buttonText}>Submit</Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress={() => this.cancel()} style={[styles.close,styles.button]}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    modal: {
        marginTop:50,
        marginLeft:20,
        marginRight:20,
        minHeight:500,
        backgroundColor:"#A2E5F9",
        borderRadius:5,
        alignSelf:"stretch",
        padding:0,
        height:null
    },
    modalWhite: {
        marginTop:50,
        marginLeft:20,
        marginRight:20,
        minHeight:110,
        backgroundColor:"#FFF",
        borderTopLeftRadius:3,
        borderTopRightRadius:3,
        borderRadius:5,
        paddingTop:10
    },
    inputDescription: {
        margin:0,
        marginRight:10,
        marginLeft:10,
        padding:10,
        textAlign: 'left',
        textAlignVertical: 'top',
        marginBottom: 5,
        borderColor: 'gray',
        borderWidth: 1
    },
    container: {
        flex:1,
        height: 500,
        alignSelf:'stretch',
        margin:10,
        padding:10,
        backgroundColor:"#01BAEF",
        borderRadius:5,
        borderWidth:3
    },
    buttonContainer : {
        padding:10,
        bottom:0,
        height: 60,
        borderBottomLeftRadius:3,
        borderBottomRightRadius:3,
        backgroundColor:'#A2E5F9',
        flexDirection:'row'
    },
    submit: {
        backgroundColor: "#20BF55",

    },
    close: {
        backgroundColor: "#FB5D35",

    },
    buttonText: {
        fontSize:15,
        fontWeight:'bold',
        color:"#FFF"
    },
    button: {
        flex:2,
        margin:3,
        padding:3,
        height:40,
        borderRadius:3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    defaultText: {
        padding:10,
        paddingTop:5,
        paddingRight:5,
        color: "#515151",
        fontSize: 14,
        textAlign: 'left',
        marginBottom: 5
    }

});
