import React from 'react';
import { StyleSheet, View, Dimensions, Text, TouchableHighlight } from 'react-native';
import ImageSlider from 'react-native-image-slider';
const infoGraphicImages = [
    "https://s3.amazonaws.com/ashtre/01.jpg",
    "https://s3.amazonaws.com/ashtre/02.jpg",
    "https://s3.amazonaws.com/ashtre/03.jpg",
    "https://s3.amazonaws.com/ashtre/04.jpg",
    "https://s3.amazonaws.com/ashtre/05.jpg",
    "https://s3.amazonaws.com/ashtre/06.jpg",
    "https://s3.amazonaws.com/ashtre/07.jpg",
    "https://s3.amazonaws.com/ashtre/08.jpg",
    "https://s3.amazonaws.com/ashtre/09.jpg"
];
const {height, width}  = Dimensions.get('window');

export default class InfoModal extends React.Component {

    constructor(props) {
        super(props);
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal() {
        this.props.closeModal();
    }

    render() {

        return (
            <View style={styles.modal}>
                <View style={styles.ImageSlider}>
                    <ImageSlider style={{

                    }} height={width-60} images={infoGraphicImages}/>
                </View>

                <TouchableHighlight
                    style={styles.button}
                    onPress={()=>this.closeModal()}>
                    <Text>Close</Text>
                </TouchableHighlight>

            </View>

        );
    }

}

const styles = StyleSheet.create({
    modal: {
        elevation: 3,
        marginTop:50,
        marginLeft:20,
        marginRight:20,
        minHeight:360,
        height:360,
        backgroundColor:"#FFF",
        borderRadius:5,
        alignSelf:"stretch",
        padding:0,
        height:null
    },

    button: {
        backgroundColor: "#20BF55",
        position:"absolute",
        bottom:3,
        margin:3,
        padding:3,
        height:40,
        borderRadius:3,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width:width-60,
    },

    ImageSlider: {
        borderRadius:5,
        height:300,
        backgroundColor:'red',
        alignItems: 'center',
        justifyContent: 'center',
        margin:5,
        zIndex:-1,
    },

});
