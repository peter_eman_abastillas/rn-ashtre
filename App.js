import React from 'react';
import { StyleSheet, AsyncStorage, StatusBar } from 'react-native';
import SplashScreen from './assets/components/SplashScreen';
import LoginScreen from './assets/components/Login/LoginScreen';
import CreatePinDetails from './assets/components/Details/CreatePinDetails';
import LoadingScreen from './assets/components/LoadingScreen';
import * as firebase from "firebase";
import Firebase from './assets/config/Firebase';

Firebase.initialise();

import {
    StackNavigator, NavigationActions
} from 'react-navigation';

const StackApps = StackNavigator({
    LoadingScreen: { screen: LoadingScreen, navigationOptions: {
            header: null
        }
    },
    LoginScreen: { screen: LoginScreen, navigationOptions: {
        header: null
    }},
    SplashScreen: { screen: SplashScreen, navigationOptions: {
        header: null
        }
    },
    CreatePinDetails: { screen: CreatePinDetails, navigationOptions: {
        header: null
    }},
});


export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: null,
            loading:false,
            user: null
        }
    }

    _navigateTo(routeName, params={}) {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({
                routeName: routeName,
                params: params
            })],
        });
        this.navigator.dispatch(resetAction);
        //this.props.navigation.dispatch(resetAction)
    }

    async componentDidMount() {
        var _this = this;
        this.setState({
            loading: true
        });
        AsyncStorage.getItem('userData').then((user_data_json) => {
            let userData = JSON.parse(user_data_json);
            console.debug(userData && !userData.uid);
            console.debug(userData);

            if(userData && !userData.uid) {
                _this._navigateTo("LoginScreen");
                return;
            }

            this.setState({
                user: userData,
                loading: false
            });
            _this._navigateTo("SplashScreen", {user:userData});

        });

    }
    render() {
            return (
                <StackApps
                    onNavigationStateChange={(prevState, currentState) => {
                        StatusBar.setHidden(true, false);
                    }} user={this.state.user} styles={styles.container} ref={nav => { this.navigator = nav; }} />
            );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight,
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
